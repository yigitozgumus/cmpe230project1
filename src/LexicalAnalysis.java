import java.io.BufferedReader;
import java.io.FileReader;


/** This class makes the lexical analysis of the source code and creates a
 *  LinkedList of Tokens.
 *  @author yigitozgumus
 * */
public class LexicalAnalysis extends Compiler {

    //Reader object of the class
    private BufferedReader codeReader;

    //Current character for scanning operation
    private char Current ;

    //EOF marker for the statement checks
    private static final char EOF = (char) (-1);


    /** Constructor for the Lexical Analysis class. Creates a LexicalAnalysis object and
     *  initiliazes BufferedReader and fileReader objects which are created for the character
     *  reading from the input file.
     *  Takes a string input as the name of file
     *  @param inputFile Name of the mylang source code file.
     * */
    public LexicalAnalysis(String inputFile){
        //try catch block for file exception
        try{
            codeReader = new BufferedReader(new FileReader(inputFile));
        }catch (Exception e){
            e.printStackTrace();
        }
        Current = read();
    }
    /** Simple read method for the codeReader object. It makes the the object read the
     *  next character surrounding with try catch block. Returns either the next char or
     *  if object has no other char to read it returns a EOF character which is declared and
     *  initialized at the beginning of the class.
     *  @return char/EOF next character possibilities.
     * */
    private char read() {
        //try catch block for reading character
        try{
            return (char) (codeReader.read());
        }catch (Exception e){
            e.printStackTrace();
            return EOF ;
        }
    }

    /** This method checks the current character for whether it is numerical or not.
     *  It has one input parameter and returns a boolean value.
     *  @param c input char of the method.
     *  @return bool is it a digit or not.
     *
     * */
    private boolean isCharNumeric(char c){
        return c >= '0' && c <= '9';
    }
    /** This method checks the current character for whether it is a char or not.
     *  Takes one input parameter and returns a boolean value.
     *
     *  @param c input char of the method.
     *  @return bool is it a char or not.
     * */
    private boolean isCharacter(char c){
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
    /** This method checks the next token of the current one and according to different
     * scenarios it returns a possible token for the token arraylist.
     * This scenerios consists of a bunch of switch and if else blocks. beware.
     * it has no input parameter and returns various types of tokens or in appropriate cases nothing.
     * */
    public Token getNextToken(){

        //state of the method
        int state = 1 ;
        //buffer for number literals which sends data to token object
        int numBuffer = 0;
        //Same as number buffer for Alpha numericals
        String alphaBuffer = "";
        //checker for skipping
        boolean skipped = false;
        //main loop
        while(true){
            if(Current == EOF && !skipped ){
                skipped = true ;
            }else if (skipped){
                try{
                    codeReader.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
                return null;
            }
            //Switch statement that changes states
            switch (state){
                //Controller case
                case 1:
                    switch(Current){
                        //sub cases whitespaces and escape characters
                        case ' ': // WhiteSpace
                        case '\n': //Newline
                        case '\b': //Beginning of the word
                        case '\f': // Form feed
                        case '\r': //Carriage return
                        case '\t': //Tab character
                            Current = read();
                            continue;
                        //Cases for various operators and markers in the language
                        case ';':
                            Current = read();
                            return new Token("SC", ";");

                        case '+':
                            Current = read();
                            return new Token("PO", "+");

                        case '-':
                            Current = read();
                            return new Token("MO", "-");

                        case '*':
                            Current = read();
                            return new Token("MUO", "*");

                        case'(':
                            Current=read();
                            return new Token("LP","(");

                        case'/':
                            Current=read();
                            return new Token("DO","/");

                        case')':
                            Current=read();
                            return new Token("RP",")");

                        case'%':
                            Current=read();
                            return new Token("MOD","%");

                        case'=':
                            Current=read();
                            return new Token("AO","=");

                        default:
                            state = 2; // Check the next possibility
                            continue;

                    }

                case 2:
                    //finding integer
                    if (isCharNumeric(Current)) {
                        numBuffer = 0; // Reset the buffer.
                        numBuffer += (Current - '0');

                        state = 3;

                        Current = read();

                    } else {
                        state=5; //does not start with number or symbol go to case 5
                    }
                    continue;

                    // Integer - Body
                case 3:
                    if (isCharNumeric(Current)) {
                        numBuffer *= 10;
                        numBuffer += (Current - '0');

                        Current = read();

                    } else {
                        return new Token("INT", "" + numBuffer);
                    }

                    continue;

                    //identifier -start
                case 5:
                    if(isCharacter(Current)|| Current=='_'){
                        alphaBuffer = "";
                        alphaBuffer+=Current;
                        state=6;
                        Current = read();
                    }else {
                        alphaBuffer = "";
                        alphaBuffer+=Current;
                        Current=read();
                        return new Token("ERROR", "Invalid input:"+alphaBuffer);
                    }
                    continue;

                case 6:
                    if ((isCharacter(Current) || isCharNumeric(Current) || Current=='_')) {

                        alphaBuffer += Current;
                        Current = read();


                    } else {

                        if( alphaBuffer.equals("do")||
                                alphaBuffer.equals("begin")||
                                alphaBuffer.equals("end")||
                                alphaBuffer.equals("if")||
                                alphaBuffer.equals("then")||
                                alphaBuffer.equals("read")||
                                alphaBuffer.equals("print")||
                                alphaBuffer.equals("while")
                                ){
                            return new Token("RW", "" + alphaBuffer);

                        }else if(alphaBuffer.equals("div")){
                            return new Token("DO", "div");
                        }

                        return new Token("ID", "" + alphaBuffer);
                    }

            }
        }
    }



}
