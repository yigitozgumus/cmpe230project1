

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/** This class will handle the parsing section of the compiler. It checks the code for
 * syntax errors and while it checks it also generates Assembly codes to a file. if the
 * source code has a correct syntax, then it exits without any error.
 * @author yigitozgumus
 *
 * */
public class RDParser extends Compiler {

    //Data fields of the class
    static LinkedList<Token> TokenList ;
    static Token LookAhead ;
    static Token postFix ;
    LinkedList<Token> postFixList ;
    FileWriter writeToFile;
    BufferedWriter toFile;
    boolean isReading = false;
    boolean isWriting = false;
    int ifLabelCounter = 0;
    int whileLabelCounter = 0;
    HashMap<String, String> Variables ;
    Set<String> keys ;

    /** Constructor for the RDParser class. Takes a tokenlist as input and transfers its elements to
     * its own data structure. Then from the fileName of the mylang source code file, method creates a name
     * for the Assembly file. Then it creates a new file object. stm() method both checks the token list and
     * invokes the appropriate methods to write the correct Assembly code to the file object. if after the stm()
     * method the token list is not empty that means the program is incorrectly written and gives an error message
     * and deletes the current File object since the code will be incorrect also. If the tokenlist is empty it
     * closes the fileWriter object.
     * @param list token list version of the mylang code.
     * */
    public RDParser(LinkedList<Token> list) throws Exception{
        TokenList = new LinkedList<Token>();
        Iterator<Token> cln = list.iterator();
        while(cln.hasNext()){
            TokenList.add(cln.next());
        }
        LookAhead =TokenList.getFirst();
        postFixList = new LinkedList<Token>();
        Variables = new HashMap<String, String>();
        fileName= fileName.substring(0, fileName.length() - 2);
        fileName= fileName.concat("asm");
        File output = new File(fileName);
        if(!output.exists()){
            output.createNewFile();
        }
        writeToFile = new FileWriter(output.getAbsoluteFile());
        toFile = new BufferedWriter(writeToFile);
        toFile.write("code segment\n");
        stm();
        keys= Variables.keySet();
        PushClosure();
        if(TokenList.size() != 0){
            output.delete();
            System.err.println(fileName +"'s syntax is incorrect.");
        }else{
            System.out.println("Code generation is completed.");
        }
        toFile.close();

    }
    /** This method looks at the token list and gets the next token in the list, discarding the current one.
     *
     * */
    private Token NextToken(){
        postFix = TokenList.pop();
        if(TokenList.isEmpty()){
            LookAhead = new Token("EPSILON","-1");
        }else {
            LookAhead = TokenList.getFirst();
        }
        return postFix;
    }
    //============================================================================
    //-----------------------------Parser Methods---------------------------------
    //============================================================================
    /** The if/else if/else structures of this method is written according to the rules of mylang language
     *  If the Syntax is correct, one invocation of stm() method will parse every mylang program since even
     *  the smallest mylang program structes is generated from a single statement non-terminal.
     *  @throws IOException since it writes to a Assembly file.
     * */
    void stm() throws IOException {
        if (LookAhead.getType().equals("ID")){
            if(!Variables.containsKey(LookAhead.getToken())){
                Variables.put(LookAhead.getToken(),"v".concat(LookAhead.getToken()));
            }
            String variable = Variables.get(LookAhead.getToken());
            postFixList.addLast(NextToken());
            if(LookAhead.getType().equals("AO")){
                Token tempAssign= NextToken();
                PushAddressToVal(variable);
                expr();
                PushAssignEnd();
                postFixList.addLast(tempAssign);
            }
        }else if (LookAhead.getToken().equals("print")){
            postFixList.addLast(NextToken());
            expr();
            PushWrite();

        }else if (LookAhead.getToken().equals("read")){
            postFixList.addLast(NextToken());
            if (LookAhead.getType().equals("ID")){
                if(!Variables.containsKey(LookAhead.getToken())){
                    Variables.put(LookAhead.getToken(),"v".concat(LookAhead.getToken()));
                }
                String variable = Variables.get(LookAhead.getToken());
                PushReadIns(variable);
                postFixList.addLast(NextToken());
            }

        }else if (LookAhead.getToken().equals("if")){
            ifLabelCounter++;
            postFixList.addLast(NextToken());
            expr();
            String variable = "ifLabel" + ifLabelCounter ;
            PushIfStart(variable);
            if (LookAhead.getToken().equals("then")){
                postFixList.addLast(NextToken());
                stm();
                PushIfEnd(variable);
            }
        }else if (LookAhead.getToken().equals("while")){
            whileLabelCounter++;
            String While1 = "WhileLabl" + whileLabelCounter;
            PushWhileStart(While1);
            postFixList.addLast(NextToken());
            expr();
            if (LookAhead.getToken().equals("do")){
                whileLabelCounter++;
                String While2 = "WhileLabl" + whileLabelCounter;
                PushWhileMiddle(While2);
                postFixList.addLast(NextToken());
                stm();
                PushWhileEnd(While1,While2);
            }

        }else if (LookAhead.getToken().equals("begin")){
            postFixList.addLast(NextToken());
            opt_stms();
            if (LookAhead.getToken().equals("end")){
                postFixList.addLast(NextToken());
            }
        }

    }
    /** Mylang rule function. Only invokes stmt_list() function.
     * @throws IOException Assembly file writing check.
     * */
    void opt_stms() throws IOException {
        stmt_list();
    }
    /** Mylang rule function. It invokes stm() function and gets the semicolon if there is one.
     * @throws IOException Assembly file writing check.
     * */
    void stmt_list() throws IOException {
        stm();
        if(LookAhead.getType().equals("SC")){
            NextToken();
            // postFixList.addLast(NextToken());
            stmt_list();
        }
    }
    /** Mylang rule function. It invokes term() function and invokes moreterms if there is any.
     * @throws IOException Assembly file writing check.
     * */
    void expr() throws IOException {
        term();
        try {
            moreterms();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /** Mylang rule function. Check addition and subtraction.
     * @throws IOException Assembly file writing check.
     * */
    void moreterms() throws IOException {
        if (LookAhead.getType().equals("PO") ){
            Token tempAssign1=  NextToken();
            term();
            moreterms();
            PushPlus();
            postFixList.addLast(tempAssign1);
        }else if(LookAhead.getType().equals("MO")){
            Token tempAssign1=  NextToken();
            term();
            PushMinus();
            moreterms();
            postFixList.addLast(tempAssign1);
        }
    }
    /** Mylang rule function. It invokes factor() and morefactors() function.
     * @throws IOException Assembly file writing check.
     * */
    void term() throws IOException {
        factor();
        morefactors();
    }
    /** Mylang rule function. It checks the multiplication, division and modulus operations.
     * @throws IOException Assembly file writing check.
     * */
    void morefactors() throws IOException {
        if(LookAhead.getType().equals("MUO")){
            Token tempAssign2= NextToken();
            factor();
            morefactors();
            PushMulti();
            postFixList.addLast(tempAssign2);
        }else if(LookAhead.getType().equals("DO")){
            Token tempAssign2= NextToken();
            factor();
            morefactors();
            PushDivision();
            postFixList.addLast(tempAssign2);
        }else if(LookAhead.getType().equals("MOD")){
            Token tempAssign2= NextToken();
            factor();
            morefactors();
            PushMod();
            postFixList.addLast(tempAssign2);
        }
    }
    /** Mylang rule function. Most basic function of the language, checks whether stm, id or integer.
     * @throws IOException Assembly file writing check.
     * */
    void factor() throws IOException {
        if (LookAhead.getType().equals("LP")) {
            NextToken();
            try {
                expr();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (LookAhead.getType().equals("RP")) {
                NextToken();
            }
        } else if (LookAhead.getType().equals("ID")) {
            if(!Variables.containsKey(LookAhead.getToken())){
                Variables.put(LookAhead.getToken(),"v".concat(LookAhead.getToken()));
            }
            String variable = Variables.get(LookAhead.getToken());
            PushToVal(variable);
            postFixList.addLast(NextToken());
        } else if (LookAhead.getType().equals("INT")) {
            String value = LookAhead.getToken();
            postFixList.addLast(NextToken());
            PushInteger(value);
        }
    }
    //==============================================================================
    // --------------------- Assembly code Insertion Methods------------------------
    //==============================================================================
    /** Appropriate Assembly code insertion method. Deals with addition.
     * @throws IOException since it writes to file object
     * */
    private void PushPlus() throws IOException {
        toFile.write(" pop cx\n" +
                " pop ax\n" +
                " add ax,cx\n" +
                " push ax\n");
    }
    /** Appropriate Assembly code insertion method. Deals with subtraction.
     * @throws IOException since it writes to file object
     * */
    private void PushMinus() throws IOException {
        toFile.write(" pop cx\n" +
                " pop ax\n" +
                " sub ax,cx\n" +
                " push ax\n");
    }
    /** Appropriate Assembly code insertion method. Deals with multiplication.
     * @throws IOException since it writes to file object
     * */
    private void PushMulti() throws IOException {
        toFile.write(" pop cx\n" +
                " pop ax\n" +
                " mul cx\n" +
                " push ax\n");
    }
    /** Appropriate Assembly code insertion method. Deals with integers.
     * @throws IOException since it writes to file object
     * */
    private void PushInteger(String value) throws IOException {
        if(value.equals("0")){
            toFile.write(" push " + value + "\n");
        }else{
            toFile.write(" push " + value + "d\n");
        }
    }
    /** Appropriate Assembly code insertion method. Deals with division.
     * @throws IOException since it writes to file object
     * */
    private void PushDivision() throws IOException {
        toFile.write(" mov dx,0\n" +
                " pop cx\n" +
                " pop ax\n" +
                " div cx\n" +
                " push ax\n");
    }
    /** Appropriate Assembly code insertion method. Deals with assignment.
     * @throws IOException since it writes to file object
     * */
    private void PushAddressToVal(String variable) throws IOException {
//        if(firstTime){
//            toFile.write(" mov "+ variable+",dx\n");
//        }

        if(Variables.containsKey(variable.substring(1))){
            toFile.write(" push offset " + variable +"\n");
        }
    }
    /** Appropriate Assembly code insertion method. Deals with assignment.
     * @throws IOException since it writes to file object
     * */
    private void PushAssignEnd() throws IOException {
        toFile.write(" pop ax\n" +
                " pop bx\n" +
                " mov [bx],ax\n");
    }
    /** Appropriate Assembly code insertion method. Deals with reading values.
     * @throws IOException since it writes to file object
     * */
    private void PushReadIns(String variable) throws IOException {
        toFile.write(" call read\n" +
                " mov " + variable+",cx\n");
        isReading = true;
    }
    /** Appropriate Assembly code insertion method. Deals with modulus.
     * @throws IOException since it writes to file object
     * */
    private void PushMod() throws IOException {
        toFile.write(" mov dx,0\n" +
                " pop cx\n" +
                " pop ax\n" +
                " div cx\n" +
                " push dx\n");
    }
    /** Appropriate Assembly code insertion method. Deals with assignment.
     * @throws IOException since it writes to file object
     * */
    private void PushToVal(String variable) throws IOException {
        toFile.write(" push " + variable +" w\n");
    }
    /** Appropriate Assembly code insertion method. Deals with writing.
     * @throws IOException since it writes to file object
     * */
    private void PushWrite() throws IOException {
        toFile.write(" pop ax\n" +
                " call print\n");
        isWriting = true;
    }
    /** Appropriate Assembly code insertion method. Deals with if stm then stm structure.
     * @throws IOException since it writes to file object
     * */
    private void PushIfStart(String variable) throws IOException {
        toFile.write(String.format(" pop ax\ncmp ax,0\n if z jmp %s\n", variable));
    }
    /** Appropriate Assembly code insertion method. Deals with if stm then stm structure.
     * @throws IOException since it writes to file object
     * */
    private void PushIfEnd(String variable) throws IOException{
        toFile.write(variable+ ":\n");
    }
    /** Appropriate Assembly code insertion method. Deals with while expr do stm structure.
     * @throws IOException since it writes to file object
     * */
    private void PushWhileStart(String variable) throws IOException{
        toFile.write(variable+":\n");
    }
    /** Appropriate Assembly code insertion method. Deals with while expr do stm structure.
     * @throws IOException since it writes to file object
     * */
    private void PushWhileMiddle(String variable) throws IOException{
        toFile.write(String.format(" pop ax\n cmp ax,0\n if z jmp  %s\n", variable));
    }
    /** Appropriate Assembly code insertion method. Deals with while expr do stm structure.
     * @throws IOException since it writes to file object
     * */
    private void PushWhileEnd(String jump,String out) throws IOException {
        toFile.write(" jmp " + jump+"\n" +out +":"+"\n");
    }
    /** Appropriate Assembly code insertion method. Deals with the end of the file and read,write functions.
     * @throws IOException since it writes to file object
     * */
    private void PushClosure()throws IOException{
        toFile.write("int  20h\n");
        if(isReading) {
            toFile.write(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                    ";reads a nonnegative integer and puts\n" +
                    ";it in cx register \n" +
                    ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
            toFile.write("read:\n" +
                    "   MOV  CX,0\n" +
                    "morechar:\n" +
                    "   mov  ah,01h \n" +
                    "   int  21h\n" +
                    "  mov  dx,0\n" +
                    "   mov  dl,al\n" +
                    "   mov  ax,cx\n" +
                    "   cmp  dl,0D\n" +
                    "   je   myret\n" +
                    "   sub  dx,48d\n" +
                    "   mov  bp,dx\n" +
                    "   mov  ax,cx\n" +
                    "   mov  cx,10d\n" +
                    "   mul  cx \n" +
                    "   add  ax,bp\n" +
                    "   mov  cx,ax\n" +
                    "   jmp  morechar\n" +
                    "myret: \n" +
                    "   ret \n");
        }
        if(isWriting){
            toFile.write(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                    "; number in AX register is printed\n" +
                    ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                    "print: \n" +
                    "   mov    si,10d\n" +
                    "   xor    dx,dx\n" +
                    "   push   ' '  \n" +
                    "   mov    cx,1d\n" +
                    "nonzero:\n" +
                    "   div    si\n" +
                    "   add    dx,48d\n" +
                    "   push   dx\n" +
                    "   inc    cx\n" +
                    "   xor    dx,dx\n" +
                    "   cmp    ax,0h\n" +
                    "   jne    nonzero\n" +
                    "   writeloop:\n" +
                    "   pop    dx\n" +
                    "   mov    ah,02h\n" +
                    "   int    21h\n" +
                    "   dec    cx\n" +
                    "   jnz    writeloop\n" +
                    "   ret\n");
        }
        if(Variables.size()!=0){
            toFile.write(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                    "; Variables are put in this area \n" +
                    ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n");
            Iterator<String> it = keys.iterator();
            while (it.hasNext()){
                String key = it.next();
                String variable = Variables.get(key);
                toFile.write(variable+"   dw ?\n");
            }
        }
        toFile.write("code ends");
    }
}