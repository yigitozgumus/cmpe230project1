
import java.io.File;
import java.util.LinkedList;


/** This class is the main class of the program. Whole processes are connected in this
 * class and final is implemented here. Other then its main method, other two methods
 * make function calls from connected classes.
 * @author yigitozgumus
 *
 * */
class Compiler {

    // Data fields
    static File inputFile = null;
    static LinkedList<Token> TokenStorage ;
    static LinkedList<Token> ParsedTokens ;
    static String fileName ;

/** Main method of the class, it creates tokens from the input mylang file and invokes
 *  ParseCode method to check the syntax of the mylang language and generate Assembly code
 *  It takes a String input as the name of the file and throws a general exception.
 *  @param args in this case the name of the file.
 *  @throws Exception if the args address is empty
 * */
    public static void main(String[] args) throws Exception {
        //first create tokens
        fileName = args[0];
        try {
            CreateTokens(args[0]);
        }catch (Exception e){
            e.printStackTrace();
        }
        //Generate Assembly from tokens
        ParseCode(TokenStorage);
    }

    /** This method is invoked in main method. Takes the filename as a string and
     *  creates a linkedlist of tokens using LexicalAnalysis class object. 
     *  @param fileName name of the mylang source code file.
     * */
    public static void CreateTokens(String fileName) {
        //get the file
        inputFile = new File(fileName);
        //Create Lexical Analysis object
        LexicalAnalysis LexerTest = new LexicalAnalysis(fileName);

        TokenStorage = new LinkedList<Token>();
        Token t;
        //Tokenize
        while ((t = LexerTest.getNextToken()) != null) {
            TokenStorage.add(t);
        }
        //Control Message
        System.out.println("Tokenization is completed.");
    }
    /** This method takes the list of tokens and while it checks them for the syntax,
     * it converts the list to the postfix notation and generates appropriate Assembly code.
     * Details will be explained in both document and related RDParser class. Takes a LinkedList of
     * Tokens as an input and throws general exception
     * @param List List of tokens from the mylang file
     * @throws Exception
     * */
    public static void ParseCode(LinkedList<Token> List) throws Exception {
        //Parse and generate
        RDParser parse = new RDParser(List);
        //Fod debug purposes
        ParsedTokens = parse.postFixList;
    }
}

