/**
 * Created by yigitozgumus on 3/13/15.
 */
/** This class simply responsible to store the info and the type of the token
 *  that lexer class provides. It has 2 data types. One for the actual token and one for
 *  the type of the token
 * */
public class Token {
    //content of the token
    private String token ;
    //codename of the token
    private String type ;

    //Constructor for the token object
    public Token(String type ,String token){
        this.token =token ;
        this.type =type ;
    }
    //Getter methods for the token data fields

    /** Gets the type of the token
     * @return type the type of the token(int,id,operator..etc.).
     * */
    public String getType() {
        return type;
    }
    /** Getter method for the token itselt
     * @return token the token itself
     * */
    public String getToken() {
        return token;
    }


    @Override
    public String toString(){
        return token + " -> " + type ;
    }
}
